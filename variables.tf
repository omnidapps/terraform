variable "client_id" {
  type = string
  description = "phoenixnap client_id"
}

variable "client_secret" {
  type = string
  sensitive = true
  description = "phoenixnap client_secret"
}
