terraform {
  required_providers {
    pnap = {
      source = "phoenixnap/pnap"
      version = "0.14.0"
    }
  }
}

provider "pnap" {
  client_id = var.client_id
  client_secret = var.client_secret
}

resource "pnap_server" "Test-Server-1" {
  hostname = "terraform"
  description = "A server created with terraform"
  os = "ubuntu/bionic"
  type = "s2.c1.small"
  location = "ASH"
  ssh_keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCqS8n2tueXlBH0l3i9fgDcYoS2hCkqHgO7BXEdY4IPwvjkgwm3cR8IHWWGfAmNAyYi9ePgj+XIxghLb1EVSJ7NNkWocYyt1bk7ncExB0YK/3EAPv3h7cJCJQYVhpRk93zttB3aVy5lYCDafCYCIoeYJGSgH/BEypTcqvfjz2W9QNbPWZroKXe8q9uhuy0e4sF1hb53qVnpzbMpurPnuWbUfAi7z9lQNrrSwcg51Wm2gOUri5LTWYQ1U7N/Q6rsvsGc8fGEz4oEZPOg0SThwRrSdan5nX0HpqPiepIhLFp3ep8HqIUa2n3gfMxq/4PJkOqaQ9OTAGCi0gaTPxTiX23dUthQKvQ4OIjM0pcTvNQOvua8WUdKqdo20sm2KFcE5yU26fwr7EL1sIWl4mvVhA6Ka3iikm01YUU8ONaFUCoXIjWLRbBE2b21EFiy8G9KFQ8pFb3QMo0YW8rEuwOCDTAT9+IKCwNRuvk62OrsqWXmNfp9UVQ+xptMsDry7Zel6nM= toasterbirb@tux"
  ]

  #action = "powered-on"
  provisioner "remote-exec" {
    connection {
      type = "ssh"
      user = "ubuntu"
      host = tolist(self.public_ip_addresses)[0]
      private_key = file("~/.ssh/id_rsa")
    }

    inline = [
      "sudo apt-get update -y",
      "sudo apt-get install -y sl neofetch",
      "echo \"sl ; neofetch\" >> /home/ubuntu/.bashrc",
    ]
  }
}
