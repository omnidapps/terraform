# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/phoenixnap/pnap" {
  version     = "0.14.0"
  constraints = "0.14.0"
  hashes = [
    "h1:6jEcpte50p2Fi2aKL+DauliBx+TRTMbH93kS0erYnmo=",
    "zh:210606780b5aa8cdf2b391a4e27cc9574a0159c1c03bfa207003389355779bad",
    "zh:264d0087f7584aec3ca9f67f0785ce315b634947d0f38b4dc4ccd4e37754a65d",
    "zh:5ff96814bb1e8c845ef3de171b6ddd00dfac2fdb74989e7fabcf08c6249eae5c",
    "zh:78dc42969b0ea8f27547d4e94615fe4485112198c6eef35cae6a8293d81d7379",
    "zh:827b676850ea6653c1e5831cbe3e7c0a8bf308540dfea2b4098085dff18b7686",
    "zh:9c92075b417af94aae4ca3112dbad6922b4197204bc601e5f87211499f5b8c5d",
    "zh:9d963683fa0433d5400d7623bb0cfcba92122fe0d21346d8f790005f4a3bbf2b",
    "zh:a1f60f63877bb801ecc5723564eb5aabf2a77d2dc037b9be2e0d271b66017c30",
    "zh:e6c6e56958aad6f3a9296e6941af0e137a3c77fcb84bb7388b03b63e0bf0fdd8",
    "zh:f33cd5578be2390070ef229ce5fbe9f8286ea37c9deb8d405534637c55279ff3",
    "zh:f9f5a0dd3e0d17f7b3baecad728f93f95a6ae81f0403fe1b99bb1d885ebac147",
  ]
}
